package fabian.task.task_2;

import javafx.animation.*;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

public class Task_2 extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) {

        double width = 600;
        double height = 600;

        Double[] points = {300.0,215.0, 350.0,375.0, 215.0,275.0,
                385.0,275.0, 245.0,375.0};
        Polygon triangle = new Polygon();
        triangle.getPoints().addAll(points);
        triangle.setFill(Color.RED);

        Pane p = new Pane();
        p.getChildren().add(triangle);

        BorderPane rootPane = new BorderPane();
        rootPane.setCenter(p);

        List<Transition> transitions = new ArrayList<>();
        transitions.add(moveToTop(triangle));
        transitions.add(moveToLeft(triangle));
        transitions.add(moveToBot(triangle));
        transitions.add(moveToRight(triangle));
        transitions.add(moveToStart(triangle));
        transitions.add(rotate(triangle));
        transitions.add(showDown(triangle));
        transitions.add(showUp(triangle));
        transitions.add(colors(triangle));
        transitions.add(colorsBack(triangle));

        VBox toolBox = new VBox();
        ToggleButton button = new ToggleButton("Start/Stop");
        button.setOnAction(event ->{
            ToggleButton tb = (ToggleButton) event.getSource();
            if (tb.isSelected()) {
                transitions.forEach(Animation::play);
            } else {
                transitions.forEach(Animation::pause);
            }
        });
        toolBox.getChildren().add(button);
        button.setMaxWidth(Double.POSITIVE_INFINITY);
        rootPane.setLeft(toolBox);
        rootPane.setBackground(new Background(new BackgroundFill(Color.BLACK,CornerRadii.EMPTY, Insets.EMPTY)));


        Scene scene = new Scene(rootPane, width, height);
        stage.setScene(scene);
        stage.show();
    }

    private Transition showDown(Shape node) {
        FadeTransition ft = new FadeTransition(Duration.millis(2000), node);
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setAutoReverse(false);
        ft.setDelay(new Duration(4000));
        return ft;
    }

    private Transition moveToTop(Shape node) {
        Path path = new Path();
        path.getElements().add(new MoveTo(300, 300));
        path.getElements().add(new CubicCurveTo(300, 300, 300, 100, 300, 100));
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(2000));
        pt.setPath(path);
        pt.setNode(node);
        pt.setAutoReverse(false);
        return pt;
    }
    private Transition moveToLeft(Shape node) {
        Path path = new Path();
        path.getElements().add(new MoveTo(300,100));
        path.getElements().add(new CubicCurveTo(300, 100, 500, 300, 500, 300));
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(2000));
        pt.setPath(path);
        pt.setNode(node);
        pt.setDelay(new Duration(2000));
        pt.setAutoReverse(false);
        return pt;
    }
    private Transition moveToBot(Shape node) {
        Path path = new Path();
        path.getElements().add(new MoveTo(500,300));
        path.getElements().add(new CubicCurveTo(500, 300, 300, 500, 300, 500));
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(2000));
        pt.setPath(path);
        pt.setNode(node);
        pt.setDelay(new Duration(4000));
        pt.setAutoReverse(false);
        return pt;
    }
    private Transition moveToRight(Shape node) {
        Path path = new Path();
        path.getElements().add(new MoveTo(300,500));
        path.getElements().add(new CubicCurveTo(300, 500, 100, 300, 100, 300));
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(2000));
        pt.setPath(path);
        pt.setNode(node);
        pt.setDelay(new Duration(6000));
        pt.setAutoReverse(false);
        return pt;
    }
    private Transition showUp(Shape node) {
        FadeTransition ft = new FadeTransition(Duration.millis(2000), node);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.setAutoReverse(false);
        ft.setDelay(new Duration(6000));
        return ft;
    }
    private Transition moveToStart(Shape node) {
        Path path = new Path();
        path.getElements().add(new MoveTo(100,300));
        path.getElements().add(new CubicCurveTo(100, 300, 300, 300, 300, 300));
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(2000));
        pt.setPath(path);
        pt.setNode(node);
        pt.setDelay(new Duration(8000));
        pt.setAutoReverse(false);
        return pt;
    }

    private Transition colors(Shape shape) {
        FillTransition ft = new FillTransition();
        ft.setFromValue(Color.RED);
        ft.setToValue(Color.BLUE);
        ft.setShape(shape);
        ft.setAutoReverse(true);
        ft.setDuration(Duration.millis(1000));
        ft.setDelay(new Duration(8000));
        return ft;
    }
    private Transition colorsBack(Shape shape) {
        FillTransition ft = new FillTransition();
        ft.setFromValue(Color.BLUE);
        ft.setToValue(Color.RED);
        ft.setShape(shape);
        ft.setAutoReverse(true);
        ft.setDuration(Duration.millis(1000));
        ft.setDelay(new Duration(9000));
        return ft;
    }

    private Transition rotate(Node node) {
        RotateTransition rt = new RotateTransition();
        rt.setAutoReverse(false);
        rt.setFromAngle(0);
        rt.setToAngle(360);
        rt.setDuration(Duration.millis(2000));
        rt.setNode(node);
        rt.setDelay(new Duration(2000));
        return rt;
    }
}
