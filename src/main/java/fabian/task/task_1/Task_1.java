package fabian.task.task_1;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Task_1 extends Application {
    public static void main(String[] args) {
        launch();
    }

    public void start(Stage stage) {
        double width = 600;
        double height = 600;
        double rectangleAndCircleWidth = 50;
        Group group = new Group();
        Scene scene = new Scene(group, width, height, Color.BLACK);

        Double[] points = {300.0,215.0, 350.0,375.0, 215.0,275.0,
                385.0,275.0, 245.0,375.0};
        Polygon triangle = new Polygon();
        triangle.getPoints().addAll(points);
        triangle.setFill(Color.BLUE);

        group.getChildren().add(triangle);

        Label label = new Label("homeworks are awesome");
        label.setLayoutX(215);
        label.setLayoutY(175);
        label.setMinHeight(50);
        label.setMinWidth(50);
        label.setTextFill(Color.GREEN);

        group.getChildren().add(label);

        group.getChildren().addAll(
                createSquareLine(rectangleAndCircleWidth,
                        0, 0,
                        0, rectangleAndCircleWidth,
                        (int) (width / rectangleAndCircleWidth)));

        group.getChildren().addAll(
                createSquareLine(rectangleAndCircleWidth,
                        0, 0,
                        rectangleAndCircleWidth, 0,
                        (int) (width / rectangleAndCircleWidth)));


        group.getChildren().addAll(
                createSquareLine(rectangleAndCircleWidth,
                        width-rectangleAndCircleWidth, rectangleAndCircleWidth,
                        0, rectangleAndCircleWidth,
                        (int) (height/ rectangleAndCircleWidth)));

        group.getChildren().addAll(
                createSquareLine(rectangleAndCircleWidth,
                        rectangleAndCircleWidth, height - rectangleAndCircleWidth,
                        rectangleAndCircleWidth, 0,
                        (int) (height / rectangleAndCircleWidth)));

        group.getChildren().addAll(
                createCircleLine(rectangleAndCircleWidth,
                        0, 0,
                        0, rectangleAndCircleWidth,
                        (int) (width / rectangleAndCircleWidth)));

        group.getChildren().addAll(
                createCircleLine(rectangleAndCircleWidth,
                        0, 0,
                        rectangleAndCircleWidth, 0,
                        (int) (width / rectangleAndCircleWidth)));


        group.getChildren().addAll(
                createCircleLine(rectangleAndCircleWidth,
                        width-rectangleAndCircleWidth, rectangleAndCircleWidth,
                        0, rectangleAndCircleWidth,
                        (int) (height/ rectangleAndCircleWidth)));

        group.getChildren().addAll(
                createCircleLine(rectangleAndCircleWidth,
                        rectangleAndCircleWidth, height - rectangleAndCircleWidth,
                        rectangleAndCircleWidth, 0,
                        (int) (height / rectangleAndCircleWidth)));

        stage.setScene(scene);
        stage.show();
    }
    private List<Rectangle> createSquareLine(double width, double x, double y, double xDelta, double yDelta, int count) {
        List<Rectangle> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            Color color = Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble());
            if(i%2==0) {
                list.add(createSquare(width, x + i * xDelta, y + i * yDelta, color));
            }
        }
        return list;
    }
    private List<Circle> createCircleLine(double width, double x, double y, double xDelta, double yDelta, int count) {
        List<Circle> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            Color color = Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble());
            if(i%2==1) {
                list.add(createCircle(width, x + i * xDelta, y + i * yDelta, color));
            }
        }
        return list;
    }

    private Rectangle createSquare(double width, double x, double y, Color color) {
        return createRectangle(width, width, x, y, color);
    }

    private Rectangle createRectangle(double width, double height, double x, double y, Color color) {
        Rectangle rectangle = new Rectangle(width, height);
        rectangle.setX(x);
        rectangle.setY(y);
        rectangle.setFill(color);
        return rectangle;
    }

    private Circle createCircle(double width, double x, double y, Color color) {
        Circle circle = new Circle(width / 2);
        circle.setCenterX(x+(width/2));
        circle.setCenterY(y+(width/2));
        circle.setFill(color);
        return circle;
    }
}
